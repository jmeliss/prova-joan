import React, { Component } from 'react';
import './App.css';
import Details from './components/details';
import Home from './components/home';
import { Link, Route,Switch } from 'react-router-dom';


class App extends Component {
  
  render() {
    return (
      <div className="container">
        <div className="alert alert-dark">
          <p>
            <Link to="/">
              <img width= "30px" alt="index" src="https://s3.eu-central-1.amazonaws.com/napptilus/level-test/imgs/logo-umpa-loompa.png"/>
            </Link>
            Oompa Loompa's crew
          </p>
        </div>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/:id" component={Details} />
       </Switch>
      </div>
    );
  }
}



export default App;
