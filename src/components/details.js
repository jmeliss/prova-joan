import React, { Component } from 'react';


class Details extends Component {

  constructor(props) {
    super(props)
    this.state = { details: [] }
  }

  UNSAFE_componentWillMount() {
    fetch('https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/oompa-loompas'+ this.props.location.pathname)
      .then((response) => {
        return response.json()
      })
      .then((details) => {
        this.setState({ details: details })
        //console.log(this.state.details);
      })
  }

  render() {
    return (
      <div className="container detailsView">
        <div className="row">
          <div className="col-sm-6">
              <img src={this.state.details.image} alt={this.state.details.first_name} width="100%"/>
          </div>
          <div className="col-sm-6">
              <h4>{this.state.details.first_name}</h4>
              <div className="details">
                {this.state.details.gender === 'M' ? 'Man' : 'Woman'} <br />
                {this.state.details.profession}
              </div>
              <p>{this.state.details.description}</p>
          </div>
        </div>
      </div>
    )
  }

}

export default Details
