import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { InfiniteScroll } from 'react-simple-infinite-scroll';
import ls from 'local-storage';


class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [],
      isLoading: true,
      cursor: 1,
      filtered: []
    };
    this.handleChange = this.handleChange.bind(this);
    const ls = require('localstorage-ttl');
  };

  componentDidMount() {
    this.loadMore();
  }

//Search
  handleChange(e) {
		let currentList = [];
		let newList = [];

		if (e.target.value !== "") {
		currentList = this.state.items;

		newList = currentList.filter(item => {
      const fn = item.first_name.toLowerCase();
      const prof = item.profession.toLowerCase();
      const conc = fn.concat(prof); //Concat firstName and profession
			const filter = e.target.value.toLowerCase();
			return conc.includes(filter);
		});
		} else {
			newList = this.state.items;
		}
		this.setState({filtered: newList});
	}


  loadMore = () => {
    this.setState({ isLoading: true, error: undefined })
    fetch(`https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/oompa-loompas?page=${this.state.cursor}`)
    .then(res => res.json())
    .then(
      res => {
        this.setState(state => ({
          items: this.state.items.concat(res.results),
          cursor: this.state.cursor + 1,
          isLoading: false,
          filtered: this.state.items.concat(res.results)
        }))
        //console.log(this.state);
      },
      error => {
        this.setState({ isLoading: false, error })
      }
    )
  }

  render() {
    return (
    <div>
      <div className="search-bar">
				<input type="text" className="input" onChange={this.handleChange} placeholder="Search..." />
			</div>

      <h1>Find your Oompa Loompa</h1>
      <h3>There are more than 100k</h3>
        
      <InfiniteScroll
          throttle={30}
          threshold={300}
          isLoading={this.state.isLoading}
          hasMore={true}
          onLoadMore={this.loadMore}
        >
          <div className="row">
          {this.state.filtered.length > 0
           ? this.state.filtered.map(item => (
                <div className="col-sm-4" key={item.id}>
                    <div className="card">
                      <img className="card-img-top" alt={item.id} src={item.image}/>
                      <div className="card">
                        <p className="card-name">{item.first_name}</p>
                        <div className="card-info">
                          {item.gender === 'M' ? <p>Man</p> : <p>Woman</p>} 
                          <p>{item.profession}</p>
                        </div>
                      </div>
                    </div>
                    <Link to={`${item.id}`} className="stretched-link"></Link>
                </div>
                
              ))
              : <div> <h2>No results :(</h2> </div>}
         </div>
        </InfiniteScroll>
        {this.state.isLoading && (
          <h2 className="loading">Loading...</h2>
        )}
    </div>
   );
  }
}

export default Home;
